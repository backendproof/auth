package com.myproject.auth.controller;

import com.myproject.auth.model.UserCredentials;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    @PostMapping
    public String generate(@RequestBody final UserCredentials credentials) {

        return "{\"status\":\"OK\"}";

    }
}
